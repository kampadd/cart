export interface ICart {
  id?: string;
  name?: string;
  description?: string;
  price?: number;
}

export class Cart implements ICart {
  constructor(public id?: string, public name?: string, public description?: string, public price?: number) {}
}
