import { Component, OnInit } from '@angular/core';
import { ICart } from 'app/shared/model/cart.model';
import { JhiEventManager, JhiEventWithContent } from 'ng-jhipster';


@Component({
  selector: 'jhi-cart-dashboard',
  templateUrl: './cart-dashboard.component.html',
  styleUrls: ['./cart-dashboard.component.scss']
})
export class CartDashboardComponent implements OnInit {
  cart: ICart | null = null;

  constructor(private eventManager: JhiEventManager) { }

  ngOnInit(): void {
    this.registerExpandedView();
  }

  registerExpandedView(): void {
    this.eventManager.subscribe('ListExpanded', (response: JhiEventWithContent<any>) => {
      this.cart = response.content as ICart;
    });
  }
  
}
