import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CartSharedModule } from 'app/shared/shared.module';
import { CartComponent } from './cart.component';
import { CartDetailComponent } from './cart-detail.component';
import { CartUpdateComponent } from './cart-update.component';
import { CartDeleteDialogComponent } from './cart-delete-dialog.component';
import { cartRoute } from './cart.route';
import { CartDashboardComponent } from './cart-dashboard.component';
import {MatTableModule} from '@angular/material/table';

@NgModule({
  imports: [CartSharedModule, MatTableModule, RouterModule.forChild(cartRoute)],
  declarations: [CartComponent, CartDetailComponent, CartUpdateComponent, CartDeleteDialogComponent, CartDashboardComponent ],
  entryComponents: [CartDeleteDialogComponent]
})
export class CartCartModule {}
