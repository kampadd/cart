/**
 * View Models used by Spring MVC REST controllers.
 */
package net.jojoaddison.web.rest.vm;
